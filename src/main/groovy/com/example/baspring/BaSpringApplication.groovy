package com.example.baspring

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class BaSpringApplication {

	static void main(String[] args) {
		SpringApplication.run(BaSpringApplication, args)
	}

}
